const Product = require('../models/product')

// Add a Product
module.exports.createProduct = (requestBody, res) =>{
    let newProduct = new Product({
        name: requestBody.name,
        price: requestBody.price
    })
    return newProduct.save()
        .then((product, error)=>{
            if(error){
                console.log(error)
                return false;
            }else{
                return product;
        }
    })
}

// Get all Products
module.exports.getAllProducts = ()=>{
    return Product.find({})
    .then((result) => {
        return result;
    })
}

// Get specifict Product
module.exports.getProduct = (productId) =>{
    return Product.findById(productId)
    .then((result, err)=>{
        if(err){
            console.log(err)
            return false;
        }else{
            return result;
        }
    })
}

// Delete specifict Product
module.exports.deleteProduct = (productId)=>{
    return Product.findByIdAndRemove(productId)
    .then((removeProduct, err)=>{
        if(err){
            console.log(err)
            return false;
        }else{
            return removeProduct;
        }
    })
}