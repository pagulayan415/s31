const express = require('express');
const router = express.Router();

// import productController.js
const productController = require('../controllers/productController')

// Add a Product
router.post('/create', (req, res)=>{
    productController.createProduct(req.body)
    .then(resultFromController => 
        res.send(resultFromController))
})

// Get all Products
router.get('/', (req, res) =>{
    productController.getAllProducts()
    .then(resultFromController => 
        res.send(resultFromController))
})

// Get a Specific Product
router.get('/:id', (req, res)=>{
    productController.getAllProducts(req.params.id)
    .then(resultFromController =>{
        res.send(resultFromController)
    })
})

// Delete a Specific Product
router.delete('/delete/:id', (req, res)=>{
    productController.deleteProduct(req.params.id)
    .then(resultFromController =>{
        res.send(resultFromController)
    })
})

module.exports = router;